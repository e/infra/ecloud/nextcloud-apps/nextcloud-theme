Nextcloud theme for /e/ - nextcloud V20 compatible version


# Installation

deploy theme in nextcloud/themes directory

use occ to update mimetypelist.js file and clear image cache : 

```
occ maintenance:theme:update
```


# License

The project is licensed under [AGPL](LICENSE).
